\documentclass[conference]{IEEEtran}

\usepackage{graphicx}
\usepackage{placeins}
\usepackage{hyperref}


\hyphenation{op-tical net-works semi-conduc-tor}


\graphicspath{ {./images/} }

\begin{document}
\title{Predicting Probability of Traffic Accidents Using Machine Learning Techniques}

\author{\IEEEauthorblockN{Saroj Duwal}
\IEEEauthorblockA{
  Department of Computer Science\\
  University of New Orleans\\
  New Orleans, LA 70148\\
  Email: sduwal@uno.edu}
\and
\IEEEauthorblockN{Abhishek Sapkota}
\IEEEauthorblockA{Department of Mathematics\\
  University of New Orleans\\
  New Orleans, LA 70148\\
  Email: asapkot2@uno.edu}
\and
\IEEEauthorblockN{Shaikh Arifuzzaman}
\IEEEauthorblockA{Department of Computer Science\\
  University of New Orleans\\
  New Orleans, LA 70148\\
  Email: smarifuz@uno.edu}
}

\maketitle

\begin{abstract}
Traffic accidents are one of the leading causes of death. More than 38,000 people die every year in crashes on U.S. roadways. In this paper, we try to find patterns in accidents and see if machine learning methods can be used to predict the probability of accidents. The data was obtained from the city of Denver website and consisted of records for the last ten years with over 160,000 records. Also, we used several Machine Learning algorithms such as Random Forest, Naive Bayes, Support Vector Classifier, and Multi-Layer Perceptron Classifier on this dataset. Of the machine learning models applied, Random Forest Classifier performed the best with an accuracy of 86\%. 

\begin{IEEEkeywords}
machine learning, traffic accident, random forest, denver, data analysis, visualization
\end{IEEEkeywords}
\end{abstract}

\section{Introduction}
Traffic accidents are unpredictable and are one of the leading causes of death in the United States. Over 38,000 \cite{accidents} people die from traffic accidents every year. It is also responsible for many health injuries and fractures ranging from mild to very serious cases. Apart from life injuries, it can also lead to severe economic hardship \cite{economy}. Our goal is to predict accident hotspots with high probability.

Accurate binary classification of accidents is near impossible. However, we can predict the probability of accidents at certain locations with high accuracy. This will be very helpful in finding accident hotspots and time when accidents are frequent. 

Accurately predicting traffic accidents is particularly challenging due to issues like class imbalance, and variation of model parameters from location to location. The same kind of parameters might have very different importance in different cities. For example, factors causing traffic accidents in large cities with dense populations and lower speed limits might vary differently from those in rural areas with low population density but high-speed limits \cite{yuan}. 

With the rapid development of data collection techniques and the availability of big urban datasets in recent years, predicting traffic accidents has become more realistic. Detailed weather data, public transportation information, and motor vehicle crash reports could provide valuable information for traffic accident analysis.

Various machine learning methods have been explored in the literature for the prediction of traffic accidents from real-world data. However, most strive to develop computational methods for global prediction of traffic accidents. It is important to note that the traffic intensity might not be the same in Niger than in New York. Thus, these methods can’t be used for prediction since traffic accidents owing to various other factors. Although taking the data for all times of the years for a place might provide accurate predictions for the specific place, they can’t be generalized to all places. Thus, in this paper, we explore the methods that give predictions for specific cities, starting with Denver.

As a potential application, the traffic accident prediction system based on our method can be used to help the traffic enforcement department to allocate police forces in advance of traffic accidents along with policy makers, city planners and health workers.

\section{Related Work}
Various deep learning-based methods have been explored in literature.   Ren et al \cite{deeplearning1} analyzed traffic accidents for the city of Beijing by taking spatial and temporal data. Similarly, deep learning approaches were applied for the state of Iowa by Yuan et al \cite{yuan}. By analyzing the spatial and temporal patterns of traffic accident frequency, they analyze the spatiotemporal correlation of traffic accidents. The classifiers created are highly non-linear and almost non-interpretable. 

The main reason behind using deep learning is that they give high accuracy for the prediction tasks. However, the main drawback of these models is that although the performance is better than non-deep learning methods, they are not quite interpretable. Thus, they can’t be used to assess how each feature contributes to the prediction performance and we can’t infer how these accidents can be prevented.


Our method is modeled on a particular geographical area only, i.e. the city of Denver. We chose to focus on a city because traffic accidents vary city to city based on the city’s population, road conditions, weather conditions, driving habits, public transit et cetera. For example, traffic accidents and conditions relating to accidents in New York City and New Orleans are bound to be very different. Similar approaches could be used and combined for each city to make a collection of predictors.
\section{Data}
The dataset \cite{dataset} consists of over 160,000 records and 45 parameters. The features were collected as categorical and numerical values. Categorical parameters were encoded into numerical parameters to simplify the prediction process \cite{scikit-learn}. After encoding, similar parameters were combined and some parameters were discarded as some contained missing values and some features were deemed not important \cite{kapeli}. The features selected [See Appendix \ref{feature}] are included in supplementary materials. This brought the number of features to bring down the total number of parameters to 15. For each accident, the parameters describe the location, time, date, weather conditions, light conditions, and the movement of the vehicles during that instance. The records with missing values were removed owing to the number of samples. 

\subsection{Negative Sampling}
Our initial dataset had only records for the instances when accidents happened. In the case of rare-events data like traffic accidents, one class prediction might not be useful since the incidence of those events might be affected by other variables not captured in only one class. For eg. incidence of accidents at one place twice a week does not imply the same all the time for each time of the month. Thus, we generated synthetic data to capture all the representative data for the actual problem. We generated synthetic data by performing an informative negative sampling approach \cite{yuan} in a  3:1 ratio with negative samples being 3.

Our idea behind generating negative sampling is that for each instance of an accident, there can be infinitely many cases where an accident doesn’t occur. To generate these negative samples, we have three features (time, month, and day). These three features were chosen because the change of them will generate diverse negative data. 

\begin{itemize}
  \item Time: Generate a random number between 0-23. Based on the time change the light conditions. 
  \item Month: Generate a random number between 1-12. Change the month of the dataset.
  \item Day: Generate a random number from 1-28. We chose 28 to simplify the process as 28 is the minimum number of days a month have. Based on the change in day and month, we create a new weekday. 
\end{itemize}

\subsection{Data Analysis}
Before we did any prediction, we tried to use visual cues provided by the data to guide our models. If we try to visualize all the accidents, we will get a heat map that roughly shapes like City of Denver. 

\begin{figure}[ht]

  \centering
  \includegraphics[width=8cm, height=4cm]{/heatmap.png}
  \caption{Heatmap of all accidents}

\end{figure}


The number of accidents across the years seems to be normally distributed. On a closer look, we begin to see some patterns regarding weekdays and weekends, hours of the day, the type of vehicle used et cetera. Our analysis showed that the number of traffic accidents was higher during rush hours. Between 7-9 am in the morning and 3-6 pm in the evening, traffic accidents spiked significantly. This data is also supported by the weekdays. During weekends, the number of accidents was significantly lower. 
\begin{figure}[ht]

  \centering
  \includegraphics[width=8cm, height=3cm]{/combine_images.jpg}
  \caption{Number of accidents compared with some features}

\end{figure}

\section{Prediction}
We used scikit-learn \cite{scikit-learn} library for Machine learning. We used scikit-learn as it provided the implementation of many machine learning algorithms. Our goal was to use some classification algorithm to get the accident probability. Our machine learning steps can be separated as below:
\begin{itemize}
  \item Holdout 20\% of the data to use as testing. We used Standard Scaler to scale the dataset.
  \item Use 10-fold cross-validation with StratifiedKFold.
  \item Run different machine learning algorithms.
  \item Get the accuracy from our holdout set. 
  
\end{itemize}

We tried different machine learning algorithms, such as Random Forest, Naive Bayes, Logistic Regression, Support Vector Machine, and Multilayer Perceptron Classifier. Out of these, we found Random Forest Classifier had the best accuracy. 

% \FloatBarrier
\begin{table}[ht!]
\centering
\begin{tabular}{|c|c|}
  \hline
  Model & Accuracy (in \%) \\ [1ex]
  \hline
  Logistic Regression   & 75 \\ [0.5ex]
  \hline
  Naive Bayes   & 77.23 \\  [0.5ex]
  \hline
  RandomForest   & 86.3 \\[0.5ex]
  \hline
  Support Vector Classifier   & Inconclusive \\ [0.5ex]
  \hline
  Multi-Layer Perceptron Classifier  & 75  \\ [0.5ex]
  \hline

\end{tabular}
\vspace{0.2cm}
\caption{Machine Learning Algorithm}
\end{table}
% \FloatBarrier
Our approach is comparable to other deep learning, SVM, and one class approaches. In our case tree-based approach worked best. As tree-based methods are formed by averaging decision trees, they are much more interpretable compared to other models. Deep learning has been used too with regards to predicting traffic accidents \cite{yuan}. However, those methods are not interpretable. Rather than just predicting probability for accidents, modeling how each feature contributes to the accidents might help for early prediction and avoidance of those accidents for policymakers.

\section{Result}
Out of all the methods, Random Forest gave us the best result. Random Forest \cite{randomforest}  is a nonlinear tree-based machine learning classifier that learns high level representations of the data. It works well when the data is non linearly separable. The random forest applies the technique of bagging to tree learners. For a training set X with responses Y, bagging B times selects a random sample with replacement of the training set and fits trees to these samples.In contrast to weakly nonlinear classifiers, Random Forest learns a decision boundary which is not linear. Moreover, since prediction from the decision trees in the Random Forest model gives an averaged prediction of the trees, the decision boundary is more nonlinear in contrast to other Kernel based methods like SVM \cite{svm}, gaussian Classifier \cite{gc} . Likewise, since trees select the best number of features and samples in the training process automatically, it makes the model interpretable in contrast to deep learning \cite{deeplearning} approaches.

\FloatBarrier
\begin{table}[h!]
  \centering
  \begin{tabular}{llll}
   & Precision & Recall & f1-Score \\ [1ex]
  0 & 0.86 & 0.98 & 0.92 \\ [0.5ex]
  1 & 0.88 & 0.54 & 0.67 \\ [0.5ex]
  &&& \\
  Accuracy &  &  & 0.87 \\ [0.5ex]
  Marco Avg & 0.87 & 0.76 & 0.79 \\ [0.5ex]
  Weighted Avg & 0.87 & 0.87 & 0.85 \\ [0.5ex]
  
  &&& \\

  Confusion Matrix &  &  &  \\ [0.5ex]
  171769 & 4332 &  &  \\ [0.5ex]
  27067 & 31482 &  &  \\ [0.5ex]
  \end{tabular}
  \vspace{0.4cm}
  \caption[]{Random Forest Classifier Benchmark}
  \end{table}
  \FloatBarrier

As seen above, our classifier had an accuracy of 0.86 for no accident and 0.88 for accident. Similarly, it had a recall of 0.98 for no accident and 0.54 for accident with f1-Score of 0.92 and 0.67 respectively. The weighted average accuracy calculated to be 0.87. The best parameters were found using grid search CV \cite{scikit-learn} for ten fold cross validation and were: gini for criterion, max\_features: auto, n\_estimators: 1000, class\_weight: balanced. 

\begin{figure}[ht!]

  \centering
  \includegraphics[width=8cm, height=6cm]{/features.png}
  \caption{Importance of Feature}

\end{figure}

From the figure above, we can see that road location has the highest feature importance. The results are in agreement with the real-world scenario that the road condition is the most responsible for traffic accidents. For eg., traffic accidents happen more in the junctions rather than straight roads. Latitude is seen to have the second-highest feature importance. Latitude is directly related to the location of the place and also is directly related to other road-related features. In the hill areas, the roads are more curved around the areas where there are high hills and the roads are straight in the plain areas. Thus it seems that the latitude of the roads, which corresponds to the geographical topography in Denver contributes second to the traffic accidents. Similarly, the travel direction of the first driver, human contributing factor, and light conditions have relatively the same level of importance. It is not surprising considering travel direction has relatively high importance as these directly correlate to accidents. It, however, had lower importance than road location and latitude as the data relating to roads seems to have the highest contribution to accidents. This concurs with real-world observation that accidents are prone in areas with curvy and bad road conditions. Human contributing factors and light conditions being important is obvious as human action and visibility are directly related to accidents. Other than that other features have somewhat equal importance as a feature.

\section{Visualization}
We have created two seperate visualization tool to analyze and predict the data. Because of computation limitation, only data from 2019 was used. The code is publicly available in \href{https://gitlab.com/sduwal/big\_data}{GitLab}.


\subsection{Data Analysis Tool}
In order to visualize all traffic accidents, we built an analysing tool for accidents. An user can select differnt time, locations and other features to filter out the data. User can also view all information about the data by clicking on each individual point. 

\FloatBarrier
\begin{figure}[h!]
  \centering
    \includegraphics[width=8cm, height=4cm]{data.png}
  \caption{Data Analysis Tool}
  \label{}
\end{figure}
\FloatBarrier

\subsection{Accident Prediction}
In order to make prediction interactive and easier, we built a web based tool. The user can select any route and click on any point of the map to get accident probability.
\FloatBarrier
\begin{figure}[h!]
  \centering
    \includegraphics[width=8cm, height=4cm]{accident.png}
  \caption{Prediction Tool}
  \label{}
\end{figure}
\FloatBarrier

\section{Conclusion}
From the results above, we can see that Random Forest Classifier performs best with an overall accuracy of 86\%. It performs better than other approaches like Naive Bayes Classifier, Logistic Regression, and other classifiers. We attribute this to the nonlinear characteristics of the algorithm due to which it learns various nonlinear representations of the data. Similarly, our method is desirable to the deep learning methods which boast high accuracy due to the model interpretability. 

However, our method has some limitations. While building the negative dataset, synthetic dataset was created and the dataset might be different from the real world data where many accidents are unaccounted for. Moreover, various features related to weather, driving conditions, and other human-related aspects have not been explored enough which might contribute to the incidence of various accidents. 

Our tool can be used by the City of Denver for various purposes. For example, the department of transportation can erect more cameras and signs around the accident hotspots. Moreover, for the drivers, the software can be built so that they are alerted when around the hotspots and when the probability for accidents is high. Likewise, similar software can be installed for self-driving cars so that accidents can be minimized.
\section{Future Work}
In order to make our method more comprehensive, we plan to do the following future work.

\begin{itemize}

    \item Integrate extensive weather data. We plan to use open weather api to gain historical weather data. We believe weather will be a vital feature while predicting traffic accidents.
    \item Integrate data on road distractions such as billboards.
    \item Integrate data on speed limits, school zones, road under construction zones, vehicle conditions.
    \item Generate clusters for selected hotspots and cluster accidents on a selected route.
    \item Improve style in visualization and automate the fields. This will improve the website and make prediction more intuitive.
\end{itemize}


In addition to doing the given work above, we would like to engineer new features to better represent the state of roads in our data.


% #REFERENCES
\bibliographystyle{IEEEtran}
\bibliography{IEEEabrv,biblio_rectifier}

\newpage
\appendices
\section{Selected Features}
\label{feature}
\begin{enumerate}
  \item GEO-X
  \item GEO-Y
  \item BICYCLE\_IND	
  \item PEDESTRIAN\_IND	
  \item ROAD\_LOCATION
  \item ROAD\_DESCRIPTION
  \item ROAD\_CONTOUR	
  \item ROAD\_CONDITION	
  \item LIGHT\_CONDITION	
  \item TU1\_VEHICLE\_TYPE	\item TU1\_TRAVEL\_DIRECTION	
  \item TU1\_DRIVER\_HUMANCONTRIBFACTOR	
  \item Weekday	
  \item hour	
  \item month	
  \item day	
  \item year	
  \item ISACCIDENT		

\end{enumerate}



\end{document}



