from flask import Flask, request
import joblib
import json
from datetime import datetime
from flask_cors import CORS, cross_origin
import logging

logging.basicConfig(level=logging.INFO)
logging.getLogger('flask-cors').level = logging.DEBUG
app = Flask(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'
cors = CORS(app, supports_credentials=True,
            resources={r"/*": {"origins": "*"}})
clf = joblib.load("modeltemp.sav")
scaler = joblib.load("Scaler")


@app.route("/")
def home():
    return "OK"


@app.route("/predict", methods=['POST'])
def predict():
    data = []
    date = datetime.now()

    req_data = request.get_json()
    print(req_data)
    data.append(req_data["GEO_X"])
    data.append(req_data["GEO_Y"])
    data.append(req_data["ROAD_LOCATION"])
    data.append(req_data["ROAD_DESCRIPTION"])
    data.append(req_data["ROAD_CONTOUR"])
    data.append(req_data["ROAD_CONDITION"])
    data.append(req_data["LIGHT_CONDITION"])
    data.append(req_data["VEHICLE_TYPE"])
    data.append(req_data["TRAVEL_DIRECTION"])
    data.append(req_data["HUMAN_FACTOR"])
    data.append(date.weekday())
    data.append(date.hour)
    data.append(date.month)
    data.append(date.day)

    print(data)
    scaler.fit([data])
    b = clf.predict_proba([data])
    print(b)
    return {"prob": str(b[0][1])}
