const mapquest = L.mapquest;
const KEY = "eYfh5GoeEaG5nk3nAZFFunG9ffcw4ASY";
mapquest.key = KEY;
let marker = null;

let latitude = null;
let longitude = null;

let map = L.mapquest.map("map", {
  center: [39.7392, -104.9903],
  layers: L.mapquest.tileLayer("light"),
  zoom: 10,
  zoomControl: true,
  attributionControl: true,
  minZoom: 5,
  maxZoom: 50,
  dragging: true,
  doubleClickZoom: false,
});

L.control
  .layers({
    Light: mapquest.tileLayer("light"),
    Map: mapquest.tileLayer("map"),
    Hybrid: mapquest.tileLayer("hybrid"),
    Satellite: mapquest.tileLayer("satellite"),
    Dark: mapquest.tileLayer("dark"),
  })
  .addTo(map);

map.addControl(L.mapquest.control());

//let button = document.getElementById('open-modal')
let modal = document.getElementById("myModal");

let form = document.getElementById("form");
//button.onclick = function(){
//  modal.style.display = "block";
//}

//TODO
document.getElementById("predict").onclick = function (e) {
  e.preventDefault();
  predictProb();
};

map.on("click", function (e) {
  // console.log(e.latlng);
  if (marker !== null) {
    map.removeLayer(marker);
  }
  latitude = e.latlng.lat;
  longitude = e.latlng.lng;
  marker = L.marker([e.latlng.lat, e.latlng.lng], {
    icon: L.mapquest.icons.marker(),
    draggable: false,
  }).addTo(map);
  marker.bindPopup(form, {
    keepInView: false,
    closeButton: true,
  });
});

const predictProb = function () {
  const url = "http://localhost:5000/predict";
  console.log(document.getElementById("time"));
  let request = {
    GEO_X: latitude,
    GEO_Y: longitude,
    ROAD_LOCATION: document.getElementById("road-location").selectedIndex,
    ROAD_DESCRIPTION: document.getElementById("road-description").selectedIndex,
    ROAD_CONTOUR: document.getElementById("road-contour").selectedIndex,
    ROAD_CONDITION: document.getElementById("road-condition").selectedIndex,
    LIGHT_CONDITION: document.getElementById("light-condition").selectedIndex,
    VEHICLE_TYPE: document.getElementById("vehicle-type").selectedIndex,
    TRAVEL_DIRECTION: document.getElementById("travel-direction").selectedIndex,
    HUMAN_FACTOR: document.getElementById("driver-factor").selectedIndex,
  };

  request = JSON.stringify(request);

  console.log(request);
  fetch(url, {
    headers: {
      "Content-Type": "application/json",
    },
    body: request,
    method: "POST",
  }).then((res) => {
    res
      .json()
      .then((data) => (document.getElementsByClassName("result-answer")[0].innerHTML = data.prob));
  });
};
let directionsControl = L.mapquest
  .directionsControl({
    className: "",
    directions: {
      options: {
        timeOverage: 25,
        doReverseGeocode: false,
      },
    },
    directionsLayer: {
      startMarker: {
        title: "Drag to change location",
        draggable: true,
        icon: "marker-start",
        iconOptions: {
          size: "sm",
        },
      },
      endMarker: {
        draggable: true,
        title: "Drag to change location",
        icon: "marker-end",
        iconOptions: {
          size: "sm",
        },
      },
      viaMarker: {
        title: "Drag to change route",
      },
      routeRibbon: {
        showTraffic: true,
      },
      alternateRouteRibbon: {
        showTraffic: true,
      },
      paddingTopLeft: [450, 20],
      paddingBottomRight: [180, 20],
    },
    startInput: {
      compactResults: true,
      disabled: false,
      location: {},
      placeholderText: "Starting point or click on the map...",
      geolocation: {
        enabled: true,
      },
      clearTitle: "Remove starting point",
    },
    endInput: {
      compactResults: true,
      disabled: false,
      location: {},
      placeholderText: "Destination",
      geolocation: {
        enabled: true,
      },
      clearTitle: "Remove this destination",
    },
    addDestinationButton: {
      enabled: false,
      maxLocations: 10,
    },
    routeTypeButtons: {
      enabled: false,
    },
    reverseButton: {
      enabled: true,
    },
    optionsButton: {
      enabled: false,
    },
    routeSummary: {
      enabled: true,
      compactResults: false,
    },
    narrativeControl: {
      enabled: false,
    },
  })
  .addTo(map);

directionsControl.setStart({
  street: "1555 Blake St",
  city: "Denver",
  county: "Denver",
  state: "CO",
});

directionsControl.setFirstDestination({
  latLng: {
    lat: 39.750307,
    lng: -103.999472,
  },
});

// const directions = L.mapquest.directions();
// directions.setLayerOptions({
//   startMarker: {
//     icon: "circle",
//     iconOptions: {
//       size: "sm",
//       primaryColor: "#1fc715",
//       secondaryColor: "#1fc715",
//       symbol: "A",
//     },
//   },
//   endMarker: {
//     icon: "circle",
//     iconOptions: {
//       size: "sm",
//       primaryColor: "#e9304f",
//       secondaryColor: "#e9304f",
//       symbol: "B",
//     },
//   },
//   routeRibbon: {
//     color: "#2aa6ce",
//     opacity: 1.0,
//     showTraffic: false,
//   },
//   alternateRouteRibbon: {
//     opacity: 1.0,
//   },
// });
// const createDirection = (start, end) => {
//   directions.route({
//     start: start.name,
//     end: end.name,
//   });
// };

// const start = placeSearch({
//   key: KEY,
//   container: document.querySelector("#place-search-input-start"),
//   collection: ["poi", "airport", "address", "adminArea"],
// });

// const end = placeSearch({
//   key: KEY,
//   container: document.querySelector("#place-search-input-end"),
//   collection: ["poi", "airport", "address", "adminArea"],
// });

// let getDirection = document.getElementById("directionsButton");
// let startAddress = "";
// let endAddress = "";

// start.on("change", (e) => {
//   startAddress = e.result;
// });

// end.on("change", (e) => {
//   endAddress = e.result;
// });

// getDirection.addEventListener("click", function (e) {
//   e.preventDefault();

//   if (startAddress == "" || endAddress == "") {
//     alert("Empty address");
//   } else {
//     createDirection(startAddress, endAddress);
//   }
// });
