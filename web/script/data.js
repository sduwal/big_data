const mapquest = L.mapquest;
const KEY = "eYfh5GoeEaG5nk3nAZFFunG9ffcw4ASY";
mapquest.key = KEY;

let address;
let markerLayers = new L.LayerGroup();

let map = L.mapquest.map("map", {
  center: [39.7392, -104.9903],
  layers: L.mapquest.tileLayer("light"),
  zoom: 10,
  zoomControl: true,
  attributionControl: true,
  minZoom: 5,
  maxZoom: 15,
  dragging: true,
  doubleClickZoom: false,
  preferCanvas: true,
});

L.control
  .layers({
    Light: mapquest.tileLayer("light"),
    Map: mapquest.tileLayer("map"),
    Hybrid: mapquest.tileLayer("hybrid"),
    Satellite: mapquest.tileLayer("satellite"),
    Dark: mapquest.tileLayer("dark"),
  })
  .addTo(map);

d3.csv("../2019.csv")
  .then(function (data) {
    return data;
  })
  .then(function (data) {
    for (let i = 0; i < data.length; i++) {
      address = data[i];
      let marker = L.circleMarker([data[i].GEO_LAT, data[i].GEO_LON], {
        color: "#FF0000",
        radius: 2,
        opacity: 0.1,
      });

      marker.bindPopup(
        `${data[i].INCIDENT_ADDRESS} <br> ${data[i].FIRST_OCCURRENCE_DATE} <br> <button onclick="describe(address)">See Details</button>`
      );
      markerLayers.addLayer(marker);
      // marker.addTo(map);
    }
    markerLayers.addTo(map);
  });

document.getElementById("clear-map").onclick = function (e) {
  document.getElementById("month").selectedIndex = 0;
  document.getElementById("weekday").selectedIndex = 0;
  document.getElementById("vehicle-type").selectedIndex = 0;
  document.getElementById("hour").selectedIndex = 0;
  document.getElementById("road-situation").selectedIndex = 0;
  document.getElementById("light").selectedIndex = 0;
};

document.getElementById("filter-map").onclick = function (e) {
  e.preventDefault();
  markerLayers.clearLayers();

  d3.csv("../2019.csv")
    .then(function (data) {
      return data;
    })
    .then(function (data) {
      let n = 0;
      for (let i = 0; i < data.length; i++) {
        let condition = true;
        address = data[i];
        let datetime = new Date(data[i].FIRST_OCCURRENCE_DATE);
        let date = datetime.getMonth() + 1;

        condition =
          condition &
          (document.getElementById("month").selectedIndex == date ||
            document.getElementById("month").selectedIndex == 0);

        let day = datetime.getDay() + 1;

        condition =
          condition &
          (document.getElementById("weekday").selectedIndex == day ||
            document.getElementById("weekday").selectedIndex == 0);

        let hour = datetime.getHours();
        condition =
          condition &
          (document.getElementById("hour").selectedIndex == hour - 1 ||
            document.getElementById("hour").selectedIndex == 0);

        let v = document.getElementById("vehicle-type");
        condition &=
          v.options[v.selectedIndex].value == address.TU1_VEHICLE_TYPE ||
          v.options[v.selectedIndex].value == address.TU2_VEHICLE_TYPE ||
          v.selectedIndex == 0;

        v = document.getElementById("road-situation");
        condition &=
          v.options[v.selectedIndex].value == address.ROAD_CONDITION ||
          v.selectedIndex == 0;

        v = document.getElementById("light");
        condition &=
          v.options[v.selectedIndex].value == address.LIGHT_CONDITION ||
          v.selectedIndex == 0;
        //draw
        if (condition) {
          n++;
          let marker = L.circleMarker([data[i].GEO_LAT, data[i].GEO_LON], {
            color: "#FF0000",
            radius: 2,
            opacity: 0.1,
          });
          marker.bindPopup(
            `${data[i].INCIDENT_ADDRESS} <br> ${data[i].FIRST_OCCURRENCE_DATE} <br> <button onclick="describe(address)">See Details</button>`
          );
          markerLayers.addLayer(marker);
        }
      }
      document.getElementById("number").innerHTML = `Number of Accidents: ${n}`;
      markerLayers.addTo(map);
    });
};

function describe() {
  let info = `Date: ${address.FIRST_OCCURRENCE_DATE} <br> Reported Date:${
    address.REPORTED_DATE
  } <br> Incident Address: ${capitalize(
    address.INCIDENT_ADDRESS
  )} <br> LatLng: (${address.GEO_LAT}, ${address.GEO_LON}) 
  <br> Neighbourhood: ${capitalize(address.NEIGHBORHOOD_ID)}
  <br> Road Description: ${capitalize(address.ROAD_DESCRIPTION)}
  <br> Road Contour: ${capitalize(address.ROAD_CONTOUR)}
  <br> Road Condition: ${capitalize(address.ROAD_CONDITION)}
  <br> Seriously Injured: ${Math.trunc(address.SERIOUSLY_INJURED)}
  <br> Fatalities: ${Math.trunc(address.FATALITIES)}
  <details><summary>Vehicle 1</summary>
   Vehicle Type: ${capitalize(address.TU1_VEHICLE_TYPE)}
  <br> Travel Direction: ${capitalize(address.TU1_TRAVEL_DIRECTION)}
  <br> Travel Movement: ${capitalize(address.TU1_VEHICLE_MOVEMENT)}
  <br> Driver Action: ${capitalize(address.TU1_DRIVER_ACTION)}
  <br> Human Factor: ${capitalize(address.TU1_DRIVER_HUMANCONTRIBFACTOR)}
  </details>
  <details><summary>Vehicle 2</summary>Vehicle Type: ${capitalize(
    address.TU2_VEHICLE_TYPE
  )}
  <br> Travel Direction: ${capitalize(address.TU2_TRAVEL_DIRECTION)}
  <br> Travel Movement: ${capitalize(address.TU2_VEHICLE_MOVEMENT)}
  <br> Driver Action: ${capitalize(address.TU2_DRIVER_ACTION)}
  <br> Human Factor: ${capitalize(
    address.TU2_DRIVER_HUMANCONTRIBFACTOR
  )}</details>


  
`;
  document.getElementById("des-detail").innerHTML = info;
}

function capitalize(s) {
  s = s.split(" ");
  a = [];
  s.forEach((element) => {
    a.push(element[0].toUpperCase() + element.slice(1).toLowerCase());
  });
  return a.join(" ");
}

for (let i = 0; i < 24; i++) {
  document.getElementById(
    "hour"
  ).innerHTML += `<option value=${i}>${i}</option>`;
}

const type = [
  "BICYCLE",
  "FARM EQUIPMENT",
  "MOTORCYCLE",
  "MOTORIZED BICYCLE",
  "OTHER",
  "PASSENGER CAR/VAN",
  "PASSENGER CAR/VAN WITH TRAILER",
  "PICKUP TRUCK/UTILITY VAN",
  "PICKUP TRUCK/UTILITY VAN WITH TRAILIER",
  "SCHOOL BUS",
  "SUV",
  "SUV WITH TRAILER",
  "TRANSIT BUS",
  "VEHICLE OVER 10000 LBS",
];

type.forEach((element) => {
  document.getElementById(
    "vehicle-type"
  ).innerHTML += `<option value="${element}">${element}</option>`;
});

const roadSituation = [
  "DRY",
  "DRY WITH VISIBLE ICY ROAD TREATMENT",
  "FOREIGN MATERIAL",
  "ICY",
  "ICY WITH VISIBLE ICY ROAD TREATMENT",
  "MUDDY",
  "SLUSHY",
  "SLUSHY WITH VISIBLE ICY ROAD TREATMENT",
  "SNOWY",
  "SNOWY WITH VISIBLE ICY ROAD TREATMENT",
  "WET",
  "WET WITH VISIBLE ICY ROAD TREATMENT",
];

roadSituation.forEach((element) => {
  document.getElementById(
    "road-situation"
  ).innerHTML += `<option value="${element}">${element}</option>`;
});

let light = ["DARK-LIGHTED", "DARK-UNLIGHTED", "DAWN OR DUSK", "DAY LIGHT"];

light.forEach((element) => {
  document.getElementById(
    "light"
  ).innerHTML += `<option value="${element}">${element}</option>`;
});
